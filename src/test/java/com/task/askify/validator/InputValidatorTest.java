package com.task.askify.validator;

import com.task.askify.model.QAModel;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class InputValidatorTest {

  private Validator<String, QAModel> validator = new InputValidator();
  private static final String LONG_VALUE =
      "TestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTest"
          + "TestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTest"
          + "TestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTes";

  @Test
  public void testQuestionAnswerExceedLength() {
    String invalidQuestion = LONG_VALUE + "? \"answer\"";
    Optional<QAModel> result = validator.validate(invalidQuestion);
    Assert.assertFalse(result.isPresent());

    String invalidAnswer = "Test? \"" + LONG_VALUE + "\"";
    result = validator.validate(invalidAnswer);
    Assert.assertFalse(result.isPresent());
  }

  @Test
  public void testInvalidQuestionFormat() {
    String[] invalidQuestion = {"", "?", "  ?", " ? ", "?Question?\""};
    for (String entry : invalidQuestion) {
      Optional<QAModel> result = validator.validate(entry);
      Assert.assertFalse(result.isPresent());
    }
  }

  @Test
  public void testInvalidQuestionAnswerFormat() {
    String[] invalidQuestionAnswer = {
      "Question?\"\"",
      "Question? \"\"",
      "Question? \" \"",
      "Question? \"Answer\"\"\"",
      "Question? \"Answer\"X",
      "Question? Answer",
      "Question?\"Answer\"",
      "Question? \"Answer"
    };
    for (String entry : invalidQuestionAnswer) {
      Optional<QAModel> result = validator.validate(entry);
      Assert.assertFalse(result.isPresent());
    }
  }

  @Test
  public void testValidQuestionFormat() {
    String expected = "Question";
    String[] validQuestion = {"Question ?", " Question ?  "};
    for (String entry : validQuestion) {
      Optional<QAModel> result = validator.validate(entry);
      Assert.assertTrue(result.isPresent());
      Assert.assertEquals(expected, result.get().getQuestion());
      Assert.assertTrue(result.get().getAnswers().isEmpty());
    }
  }

  @Test
  public void testValidQuestionAnswerFormat() {
    String expectedQuestion = "Question";
    List<String> expectedAnswerList =
        new ArrayList<String>() {
          {
            add("answer 1");
            add("answer 2");
          }
        };
    String[] invalidQuestionAnswer = {
      "Question? \"answer 1   \" \"    answer 2 \"",
      "  Question   ?    \"   answer 1   \"    \"answer 2\"  "
    };
    for (String entry : invalidQuestionAnswer) {
      Optional<QAModel> result = validator.validate(entry);
      Assert.assertTrue(result.isPresent());
      Assert.assertFalse(result.get().getAnswers().isEmpty());

      Assert.assertEquals(expectedQuestion, result.get().getQuestion());
      Assert.assertEquals(expectedAnswerList, result.get().getAnswers());
    }
  }
}
