package com.task.askify.validator;

import java.util.Optional;

/** Validates the state of the passed argument type. */
public interface Validator<T, E> {

  /**
   * Validates the argument by applying specific rules
   *
   * @param value validated argument
   * @return a {@link Optional<E>} data type
   */
  Optional<E> validate(T value);
}
