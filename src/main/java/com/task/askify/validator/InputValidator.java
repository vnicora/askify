package com.task.askify.validator;

import com.task.askify.model.QAModel;

import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Validates the data format and translate to a given model */
public class InputValidator implements Validator<String, QAModel> {
  private static final String QUESTION_DELIMITER = "\\?";
  private static final Pattern QUESTION_PATTERN =
      Pattern.compile("^ *(.*[^ ?\"\\t\\n] *+ *)\\? *$");
  private static final Pattern QUESTION_ANSWER_PATTERN =
      Pattern.compile("^ *(.*[^ ?\"\\t\\n] *+ *)\\?( +(\\\"( *\\w+ *)+\\\") *)+$");

  private Predicate<String> isEmpty = s -> s == null || s.trim().isEmpty();
  private final int MAX_LENGTH = 255;

  @Override
  public Optional<QAModel> validate(String value) {
    if (value == null || isEmpty.test(value)) {
      return Optional.empty();
    }

    Matcher matcher = QUESTION_PATTERN.matcher(value);
    if (matcher.matches()) {
      String question = matcher.group(1).trim();
      if(!isValidLength(question)) {
        return Optional.empty();
      }
      return Optional.of(new QAModel(question));
    }

    matcher = QUESTION_ANSWER_PATTERN.matcher(value);
    if (matcher.matches()) {
      String[] inputArr = value.split(QUESTION_DELIMITER);
      String question = inputArr[0].trim();
      if(!isValidLength(question)) {
        return Optional.empty();
      }

      QAModel model = new QAModel(question);
      boolean succeed = parseTheAnswers(inputArr[1], model);
      if (succeed) {
        return Optional.of(model);
      }
    }

    return Optional.empty();
  }

  private boolean isValidLength(String value) {
    return value.length() < MAX_LENGTH;
  }

  /**
   * @param answers string value
   * @param model data holder
   */
  private boolean parseTheAnswers(String answers, QAModel model) {
    int start = -1, end = -1;
    for (int i = 0; i < answers.length(); i++) {
      char c = answers.charAt(i);
      if (c == '"') {
        if (start == -1) {
          start = i + 1;
        } else {
          end = i;
        }

        if (start < end) {
          String answer = answers.substring(start, end).trim();
          if(!isValidLength(answer)) {
            return false;
          }
          model.addAnswer(answer);
          start = -1;
          end = -1;
        }
      }
    }
    return true;
  }
}
