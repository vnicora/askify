import com.task.askify.StartAsking;
import org.junit.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class StartAskingTest {

  private ByteArrayOutputStream testOut;

  @Before
  public void before() {
    testOut = new ByteArrayOutputStream();
    System.setOut(new PrintStream(testOut));
  }

  private void provideInput(String data) {
    ByteArrayInputStream testIn = new ByteArrayInputStream(data.getBytes());
    System.setIn(testIn);
  }

  private String getOutput() {
    return testOut.toString();
  }

  @After
  public void restoreSystemInputOutput() {
    System.setIn(System.in);
    System.setOut(System.out);
  }

  @Test
  public void testStartAskingInfo() throws IOException, InterruptedException {
    startApplicationThread("q", 2000);

    String output = getOutput();
    Assert.assertTrue(output.contains(StartAsking.START_ASKING_MESSAGE));
    Assert.assertTrue(output.contains(StartAsking.QUESTION_MESSAGE));
    Assert.assertTrue(output.contains(StartAsking.QUESTION_ANSWER_MESSAGE));
    Assert.assertTrue(output.contains(StartAsking.BYE_MESSAGE));
  }

  @Test
  public void testGenericMessage() throws IOException, InterruptedException {
    startApplicationThread("Question?", 3000);
    String output = getOutput();
    Assert.assertTrue(output.contains(StartAsking.GENERIC_ANSWER));
  }

  @Test
  public void testInvalidMessage() throws IOException, InterruptedException {
    startApplicationThread("Question?:", 3000);
    String output = getOutput();
    Assert.assertTrue(output.contains(StartAsking.GENERIC_ERR_MESSAGE));
  }

  private void startApplicationThread(String input, long delay) throws InterruptedException {
    Thread askifyThread =
        new Thread(
            () -> {
              try {
                provideInput(input);
                StartAsking.main("");
              } catch (IOException e) {
              }
            });

    askifyThread.start();
    askifyThread.join(delay);
  }
}
