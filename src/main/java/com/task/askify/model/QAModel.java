package com.task.askify.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/** Question/Answer model */
public class QAModel {
  private String question;
  private List<String> answers;

  public QAModel(String question) {
    this.question = question;
  }

  /**
   * Get question.
   *
   * @return question value
   */
  public String getQuestion() {
    return question;
  }

  /**
   * Get the list of answers.
   *
   * @return a list of answers
   */
  public List<String> getAnswers() {
    return answers == null ? Collections.emptyList() : answers;
  }

  /**
   * Add an answer.
   *
   * @param answer added into list
   */
  public void addAnswer(String answer) {
    if (answers == null) {
      answers = new ArrayList<>();
    }
    answers.add(answer);
  }

  /**
   * Add multiple answers at ones.
   *
   * @param answerList added into list
   */
  public void addAnswers(List<String> answerList) {
    if (answers == null) {
      answers = new ArrayList<>();
    }

    answers.addAll(answerList);
  }

  public boolean isQuestion() {
    return answers == null;
  }
}
