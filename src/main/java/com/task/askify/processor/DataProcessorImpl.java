package com.task.askify.processor;

import com.task.askify.StartAsking;
import com.task.askify.model.QAModel;
import com.task.askify.validator.Validator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

/** Process the data and store the result for further usage. */
public class DataProcessorImpl implements DataProcessor<String> {

  private Validator<String, QAModel> validator;
  private Map<String, List<String>> dataStorageMap;
  private Consumer<String> consumer;

  public DataProcessorImpl(Validator<String, QAModel> validator, Consumer<String> consumer) {
    dataStorageMap = new HashMap<>();
    this.validator = validator;
    this.consumer = consumer;
  }

  @Override
  public void process(String value) {
    Optional<QAModel> modelOptional = validator.validate(value);
    if (!modelOptional.isPresent()) {
      consumer.accept(StartAsking.GENERIC_ERR_MESSAGE);
      return;
    }

    QAModel model = modelOptional.get();
    if (model.isQuestion()) {
      List<String> answerList = dataStorageMap.get(model.getQuestion());
      if (answerList == null) {
        consumer.accept(StartAsking.GENERIC_ANSWER);
      } else {
        answerList.forEach(consumer);
      }
    } else {
      dataStorageMap.put(model.getQuestion(), model.getAnswers());
    }
  }
}
