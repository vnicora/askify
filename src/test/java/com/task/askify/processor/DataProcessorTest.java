package com.task.askify.processor;

import com.task.askify.StartAsking;
import com.task.askify.model.QAModel;
import com.task.askify.validator.Validator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

@RunWith(MockitoJUnitRunner.class)
public class DataProcessorTest {

  @Mock private Validator<String, QAModel> validator;
  @Mock private Consumer<String> consumer;
  private DataProcessor<String> processor;

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);
    processor = new DataProcessorImpl(validator, consumer);
  }

  @Test
  public void testProcessErrorMessage() {
    Mockito.when(validator.validate(Mockito.anyString())).thenReturn(Optional.empty());
    ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);

    processor.process("Question");
    Mockito.verify(consumer, Mockito.times(1)).accept(argument.capture());
    Assert.assertEquals(StartAsking.GENERIC_ERR_MESSAGE, argument.getValue());
  }

  @Test
  public void testProcessGenericAnswer() {
    String question = "Question";
    Mockito.when(validator.validate(Mockito.anyString()))
        .thenReturn(Optional.of(new QAModel(question)));
    ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);

    processor.process(question + "?");
    Mockito.verify(consumer, Mockito.times(1)).accept(argument.capture());
    Assert.assertEquals(StartAsking.GENERIC_ANSWER, argument.getValue());
  }

  @Test
  public void testProcessQuestionAndGiveAnswer() {
    String question = "Question";
    QAModel model = Mockito.mock(QAModel.class);
    Mockito.when(model.isQuestion()).thenReturn(false).thenReturn(true);
    Mockito.when(model.getQuestion()).thenReturn(question).thenReturn(question);
    List<String> answers = Arrays.asList("answer 1", "answer 2");
    Mockito.when(model.getAnswers()).thenReturn(answers);

    Mockito.when(validator.validate(Mockito.anyString())).thenReturn(Optional.of(model));
    ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);

    processor.process(question + "? \"answer 1\" \"answer 2\"");
    processor.process(question + "?");

    Mockito.verify(consumer, Mockito.times(2)).accept(argument.capture());
    Assert.assertEquals(answers, argument.getAllValues());
  }
}
