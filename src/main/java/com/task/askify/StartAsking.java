package com.task.askify;

import com.task.askify.processor.DataProcessor;
import com.task.askify.processor.DataProcessorImpl;
import com.task.askify.validator.InputValidator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.Consumer;

/** */
public class StartAsking {
  public static final String ADD_QUESTION_FORMAT =
      "<question>? \"<answer1>\" \"<answer2>\" \"<answerN>\"";
  public static final String ASK_QUESTION_FORMAT = "<question>?";
  public static final String GENERIC_ERR_MESSAGE =
      "Invalid question/answer format, please follow the template above!";
  public static final String GENERIC_ANSWER = "The answer to life, universe and everything is 42!";
  public static final String START_ASKING_MESSAGE =
      "Ask a question or add a question/answer of everything!";
  public static final String QUESTION_MESSAGE = "Ask question format: " + ASK_QUESTION_FORMAT;
  public static final String QUESTION_ANSWER_MESSAGE =
      "Add question/answer format: " + ADD_QUESTION_FORMAT;
  public static final String QUIT_MESSAGE = "Enter something or 'q' to quit!";
  public static final String BYE_MESSAGE = "Bye bye ...";
  public static final String[] RULES = {
    "A Question is a String with max 255 chars",
    "An Answer is a String with max 255 chars",
    "A Question can have multiple answers (like bullet points)"
  };

  public static void main(String... args) throws IOException {

    System.out.println("\n" + START_ASKING_MESSAGE);
    System.out.println("------------------- FORMATS -------------------");
    System.out.println(QUESTION_MESSAGE);
    System.out.println(QUESTION_ANSWER_MESSAGE);
    System.out.println("-----------------------------------------------");
    System.out.println(RULES[0]);
    System.out.println(RULES[1]);
    System.out.println(RULES[2]);
    System.out.println("-----------------------------------------------");
    System.out.println(QUIT_MESSAGE);
    System.out.println("-----------------------------------------------\n");

    boolean waitForInput = true;
    Consumer<String> consumer = System.out::println;
    DataProcessor<String> dataProcessor = new DataProcessorImpl(new InputValidator(), consumer);
    try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
      while (waitForInput) {
        String input = reader.readLine();
        if (input != null && !input.equals("q")) {
          dataProcessor.process(input);
        } else {
          waitForInput = false;
          System.out.println(BYE_MESSAGE);
        }
      }
    }
  }
}
