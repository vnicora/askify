package com.task.askify.processor;

/**
 * Handles the provided data type
 *
 * @param <T>
 */
public interface DataProcessor<T> {

  /**
   * Process the data type
   *
   * @param value
   */
  void process(T value);
}
